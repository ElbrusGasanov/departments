from django.db import models


class Department(models.Model):
    name = models.CharField(max_length=128)
    director = models.OneToOneField(
        to="app.Employee", 
        on_delete=models.SET_NULL, 
        blank=True, 
        null=True, 
        related_name="owned_department",
    )

    def __str__(self) -> str:
        return self.name


class Employee(models.Model):
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    middle_name = models.CharField(max_length=32, blank=True, null=True)

    photo = models.ImageField(blank=True, null=True)
    position = models.CharField(max_length=64)
    salary = models.PositiveIntegerField()
    birth_date = models.DateField()

    department = models.ForeignKey(
        to=Department, on_delete=models.PROTECT, related_name="employees"
    ) 

    def __str__(self) -> str:
        return "{} {} {} - {} - {}".format(
            self.last_name,
            self.first_name,
            self.middle_name,
            self.department,
            self.position,
        )

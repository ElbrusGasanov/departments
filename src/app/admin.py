from django.contrib import admin
from app.models  import Department, Employee
from django.contrib.auth.models import Group


class DepartmentModelAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "director_name",
    )

    def director_name(self, obj):
        return "{} {} {}".format(
            obj.director.last_name,
            obj.director.first_name,
            obj.director.middle_name,
        ) if obj.director else "-"
    
    director_name.short_description = "director"


class EmployeeModelAdmin(admin.ModelAdmin):
    list_display = (
        "last_name",
        "first_name",
        "middle_name",
        "department",
        "position",
        "salary",
        "birth_date",
        "photo",
    )


admin.site.register(Department, DepartmentModelAdmin)
admin.site.register(Employee, EmployeeModelAdmin)

admin.site.unregister(Group)

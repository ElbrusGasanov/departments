from django.urls import path
from rest_framework import routers
from app.api.views import DepartmentAPIView, EmployeeViewSet


router = routers.SimpleRouter()


router.register(r'employee', EmployeeViewSet)
urlpatterns = router.urls

urlpatterns += [
    path("department/", DepartmentAPIView.as_view())
]

from rest_framework.generics import ListAPIView
from app.models import Employee, Department
from app.api.serializers import DepartmentListSerializer, EmployeeListSerializer
from rest_framework.mixins import ListModelMixin, CreateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter
from django.db.models import Sum, Count


class DepartmentAPIView(ListAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentListSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.annotate(
            employee_count=Count('employees__id', distinct=True)
        ).annotate(
            salary_sum=Sum("employees__salary")
        )
        

class EmployeeViewSet(
    ListModelMixin,
    CreateModelMixin,
    DestroyModelMixin,
    GenericViewSet,
):
    queryset = Employee.objects.all().order_by("id")
    serializer_class = EmployeeListSerializer
    permission_classes = [IsAuthenticated,]
    pagination_class = PageNumberPagination
    filter_backends = [SearchFilter]
    search_fields = ("last_name", "department__id")

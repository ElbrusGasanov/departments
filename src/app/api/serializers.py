import datetime
from rest_framework import serializers
from app.models import Department, Employee


class DepartmentBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = (
            "id",
            "name",
            "director",
        )


class DepartmentListSerializer(DepartmentBaseSerializer):
    employee_count = serializers.IntegerField()
    salary_sum = serializers.IntegerField()

    class Meta:
        model = Department
        fields = (
            "id",
            "name",
            "director",
            "employee_count",
            "salary_sum",
        )


class EmployeeListSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField()

    class Meta:
        model = Employee
        fields = (
            "id",
            "last_name",
            "first_name",
            "middle_name",
            "photo",
            "position",
            "department",
            "age",
            "birth_date",
            "salary",
        )
        # В данных для чтения должен быть возраст вместо даты рождения
        read_only_fields = ("age",)
        # При создании пользователя нужно будет передавать его дату рождения
        extra_kwargs = {
            "birth_date": {
                "write_only": True,
            },
        }

    # Это нужно для того, чтобы при get-запросе получать сериализованный объект,
    # а при post-запросе передавать только id объекта
    def to_representation(self, instance):
        self.fields["department"] = DepartmentBaseSerializer(read_only=True)
        return super(EmployeeListSerializer, self).to_representation(instance)

    
    def get_age(self, obj) -> int:
        return datetime.datetime.now().year - obj.birth_date.year